## Backend setup
```
cd backend
```
```
npm install
```
```
create database and execute /task.sql file for creating tables
```
```
change db configs in /app/config/db.config.js
```

```
npm start
```

## Frontend setup
```
cd frontend
```
```
if you changed backend host or port,
configure it in /frontend/src/config.js
```
```
npm install
```

```
npm start
```

