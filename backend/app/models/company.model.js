const sql = require("./db.js");

// constructor
const Company = function(company) {
  this.name = company.name;
  this.created_at = new Date();
};

Company.create = (newCompany, result) => {
  sql.query("INSERT INTO company SET ?", newCompany, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    result(null, { id: res.insertId, ...newCompany });
  });
};

Company.findById = (companyId, result) => {
  sql.query(`SELECT * FROM company WHERE id = ${companyId}`, (err, res) => {
    if (err) {
      result(err, null);
      return;
    }

    if (res.length) {
      result(null, res[0]);
      return;
    }

    // not found Company with the id
    result({ kind: "not_found" }, null);
  });
};

Company.getAll = ({ offset, per_page, show_all }, result) => {
  let limit = "";

  if (!show_all) {
    limit += `LIMIT ${offset},${per_page}`;
  }

  sql.query(
    `SELECT SQL_CALC_FOUND_ROWS * FROM company ORDER BY id DESC ${limit}`,
    (err, res) => {
      if (err) {
        result(null, err);
        return;
      }

      return sql.query(
        "SELECT COUNT(*) as totalCount from company;",
        (err, [resp]) => {
          result(null, { data: res, totalCount: resp.totalCount });
        }
      );
    }
  );
};

Company.updateById = (id, company, result) => {
  sql.query(
    "UPDATE company SET name = ? WHERE id = ?",
    [company.name, id],
    (err, res) => {
      if (err) {
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        // not found Customer with the id
        result({ kind: "not_found" }, null);
        return;
      }

      result(null, { id: id, ...company });
    }
  );
};

Company.delete = (id, result) => {
  sql.query("DELETE FROM company WHERE id = ?", id, (err, res) => {
    if (err) {
      return result(null, err);
    }

    if (res.affectedRows == 0) {
      // not found Customer with the id
      result({ kind: "not_found" }, null);
      return;
    }

    result(null, res);
  });
};

Company.deleteAll = result => {
  sql.query("DELETE FROM company", (err, res) => {
    if (err) {
      result(null, err);
      return;
    }
    result(null, res);
  });
};

module.exports = Company;
