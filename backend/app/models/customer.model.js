const sql = require("./db.js");

// constructor
const Customer = function(customer) {
  Object.assign(this, { ...customer });
};

Customer.create = (newCustomer, result) => {
  sql.query("INSERT INTO customer SET ?", newCustomer, (err, res) => {
    if (err) {
      result(err, null);
      return;
    }

    result(null, { id: res.insertId, ...newCustomer });
  });
};

Customer.findById = (customerId, result) => {
  sql.query(`SELECT * FROM customer WHERE id = ${customerId}`, (err, res) => {
    if (err) {
      return result(err, null);
    }

    if (res.length) {
      return result(null, res[0]);
    }

    // not found Customer with the id
    result({ kind: "not_found" }, null);
  });
};

Customer.getAll = ({ offset, per_page }, result) => {
  sql.query(
    `SELECT SQL_CALC_FOUND_ROWS c.*,co.name as company_name
    FROM customer c INNER JOIN company co ON co.id = c.company_id 
    ORDER BY id DESC LIMIT ${offset},${per_page}`,
    (err, res) => {
      if (err) {
        result(null, err);
        return;
      }

      return sql.query(
        "SELECT COUNT(*) as totalCount from customer;",
        (err, [resp]) => {
          result(null, { data: res, totalCount: resp.totalCount });
        }
      );
    }
  );
};

Customer.updateById = (id, customer, result) => {
  sql.query(
    "UPDATE customer SET first_name = ?, last_name = ?, company_id = ? WHERE id = ?",
    [customer.first_name, customer.last_name, customer.company_id, id],
    (err, res) => {
      if (err) {
        return result(null, err);
      }

      if (res.affectedRows == 0) {
        // not found Customer with the id
        return result({ kind: "not_found" }, null);
      }

      result(null, { id: id, ...customer });
    }
  );
};

Customer.delete = (id, result) => {
  sql.query("DELETE FROM customer WHERE id = ?", id, (err, res) => {
    if (err) {
      return result(null, err);
    }

    if (res.affectedRows == 0) {
      // not found Customer with the id
      return result({ kind: "not_found" }, null);
    }

    result(null, res);
  });
};

Customer.deleteAll = result => {
  sql.query("DELETE FROM customer", (err, res) => {
    if (err) {
      return result(err, null);
    }
    result(null, res);
  });
};

Customer.search = ({ offset, per_page, q, company_id }, result) => {
  let where = " 1 ";

  if (q) {
    where += ` AND CONCAT( first_name,  ' ', last_name ) LIKE  '%${q}%'`;
  }

  if (company_id) {
    where += ` AND company_id=${company_id}`;
  }

  sql.query(
    `SELECT c.*, co.name as company_name FROM customer c 
    INNER JOIN company co ON co.id = c.company_id 
    where ${where} ORDER BY id DESC LIMIT ${offset},${per_page}`,
    (err, res) => {
      if (err) {
        return result(err, null);
      }

      return sql.query(
        `SELECT COUNT(*) as totalCount from customer where ${where}`,
        (err, [resp]) => {
          result(null, { data: res, totalCount: resp.totalCount });
        }
      );

      return result(null, res);
    }
  );
};

module.exports = Customer;
