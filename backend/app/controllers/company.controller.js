const Company = require("../models/company.model.js");
const { check, validationResult } = require("express-validator/check");
const { per_page } = require("../config/local");

exports.validate = method => {
  switch (method) {
    case "update":
    case "create": {
      return [
        check("name", "name is required!")
          .not()
          .isEmpty()
          .trim()
          .escape()
      ];
    }
  }
};

// Create and Save a new Company
exports.create = (req, res) => {
  // Finds the validation errors in this request and wraps them in an object with handy functions
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }

  // Create a Company
  const company = new Company({
    name: req.body.name
  });

  // Save Company in the database
  Company.create(company, (err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Company."
      });
    else res.status(201).json(data);
  });
};

// Find a single Company with a company_id
exports.findOne = (req, res) => {
  req.params.company_id = parseInt(req.params.company_id);

  Company.findById(req.params.company_id, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found Company with id ${req.params.company_id}.`
        });
      } else {
        res.status(500).send({
          message: "Error retrieving Company with id " + req.params.company_id
        });
      }
    } else res.status(200).json(data);
  });
};

// Retrieve all Companies from the database.
exports.findAll = (req, res) => {
  let { page = 1, show_all } = req.query;

  let offset = (page - 1) * per_page;

  Company.getAll({ offset, per_page, show_all }, (err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving companies."
      });
    else res.status(200).json(data);
  });
};

// Update a Company identified by the company_id in the request
exports.update = (req, res) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }

  Company.updateById(
    req.params.company_id,
    new Company(req.body),
    (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Not found Company with id ${req.params.company_id}.`
          });
        } else {
          res.status(500).send({
            message: "Error updating Company with id " + req.params.company_id
          });
        }
      } else res.status(200).json(data);
    }
  );
};

// Delete a Company with the specified company_id in the request
exports.delete = (req, res) => {
  Company.delete(req.params.company_id, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found Company with id ${req.params.company_id}.`
        });
      } else {
        res.status(500).send({
          message: "Could not delete Company with id " + req.params.company_id
        });
      }
    } else res.send({ message: `Company was deleted successfully!` });
  });
};

// Delete all Companies from the database.
exports.deleteAll = (req, res) => {
  Company.deleteAll((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all customers."
      });
    else res.send({ message: `All Companies were deleted successfully!` });
  });
};
