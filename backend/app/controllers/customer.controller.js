const Customer = require("../models/customer.model.js");
const Company = require("../models/company.model.js");
const { check, validationResult } = require("express-validator/check");
const { per_page } = require("../config/local");

exports.validate = method => {
  switch (method) {
    case "update":
    case "create": {
      return [
        check("first_name", "first_name is required!")
          .isString()
          .trim()
          .escape(),

        check("last_name", "last_name is required!")
          .isString()
          .trim()
          .escape(),

        check("company_id", "company is required, and must be integer.").isInt()
      ];
    }
  }
};

// Create and Save a new Customer
exports.create = (req, res) => {
  // Create a Customer

  // Finds the validation errors in this request and wraps them in an object with handy functions
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }

  const customer = new Customer({
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    company_id: req.body.company_id,
    created_at: new Date()
  });

  //checking is company exists
  Company.findById(req.body.company_id, (err, data) => {
    if (err) {
      return res.status(404).json({
        message: "Company doesnt exists"
      });
    }

    // Save Customer in the database
    Customer.create(customer, (err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the Customer."
        });
      else res.status(201).json(data);
    });
  });
};

// Find a single Customer with a customerId
exports.findOne = (req, res) => {
  Customer.findById(req.params.customer_id, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found Customer with id ${req.params.customer_id}.`
        });
      } else {
        res.status(500).send({
          message: "Error retrieving Customer with id " + req.params.customer_id
        });
      }
    } else res.status(200).json(data);
  });
};

// Retrieve all Customers from the database.
exports.findAll = (req, res) => {
  let { page = 1 } = req.query;

  let offset = (page - 1) * per_page;

  Customer.getAll({ offset, per_page }, (err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving customers."
      });
    else res.status(200).json(data);
  });
};

// Search Customers by filter from the database.
exports.search = (req, res) => {
  let { q, company_id, page = 1 } = req.query;

  let offset = (page - 1) * per_page;
  company_id = parseInt(company_id);

  Customer.search({ offset, per_page, q, company_id }, (err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving customers."
      });
    else res.status(200).json(data);
  });
};

// Update a Customer identified by the customer_id in the request
exports.update = (req, res) => {
  // Finds the validation errors in this request and wraps them in an object with handy functions
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }

  //checking is company exists
  Company.findById(req.body.company_id, (err, data) => {
    if (err) {
      return res.status(404).json({
        message: "Company doesnt exists"
      });
    }

    Customer.updateById(
      req.params.customer_id,
      new Customer(req.body),
      (err, data) => {
        if (err) {
          if (err.kind === "not_found") {
            res.status(404).send({
              message: `Not found Customer with id ${req.params.customerId}.`
            });
          } else {
            res.status(500).send({
              message:
                "Error updating Customer with id " + req.params.customerId
            });
          }
        } else res.status(200).json(data);
      }
    );
  });
};

// Delete a Customer with the specified customerId in the request
exports.delete = (req, res) => {
  Customer.delete(req.params.customer_id, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found Customer with id ${req.params.customer_id}.`
        });
      } else {
        res.status(500).send({
          message: "Could not delete Customer with id " + req.params.customer_id
        });
      }
    } else
      res.status(200).json({ message: `Customer was deleted successfully!` });
  });
};

// Delete all Customers from the database.
exports.deleteAll = (req, res) => {
  Customer.deleteAll((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all customers."
      });
    else
      res
        .status(200)
        .json({ message: `All Customers were deleted successfully!` });
  });
};
