module.exports = app => {
  const customer = require("../controllers/customer.controller.js");

  // Create a new Customer
  app.post("/customer", customer.validate("create"), customer.create);

  // Get customers by filter
  app.get("/customer/_search", customer.search);

  // Retrieve a single Customer with customerId
  app.get("/customer/:customer_id", customer.findOne);

  // Retrieve all Customers
  app.get("/customer", customer.findAll);

  // Update a Customer with customerId
  app.put(
    "/customer/:customer_id",
    customer.validate("update"),
    customer.update
  );

  // Delete a Customer with customerId
  app.delete("/customer/:customer_id", customer.delete);

  // Create a new Customer
  app.delete("/customer", customer.deleteAll);
};
