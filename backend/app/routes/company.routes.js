module.exports = app => {
  const company = require("../controllers/company.controller.js");

  // Create a new company
  app.post("/company", company.validate("create"), company.create);

  // Update a company with companyId
  app.put("/company/:company_id", company.validate("update"), company.update);

  // Retrieve a single company with companyId
  app.get("/company/:company_id", company.findOne);

  // Retrieve all companies
  app.get("/company", company.findAll);

  // Delete a company with companyId
  app.delete("/company/:company_id", company.delete);

  // Create a new company
  app.delete("/company", company.deleteAll);
};
