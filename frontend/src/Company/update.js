import React from "react";
import axios from "axios";
import { API_HOST } from "../config";

export default class CompanyUpdate extends React.Component {
  state = {
    errors: []
  };

  constructor(props) {
    super(props);

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  validateForm() {
    let errors = [];

    let { name } = this.state;

    if (!name) {
      errors.push("Name is required!");
    }

    return errors;
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    let errors = this.validateForm();
    console.log("*********** CONSOLE **********");
    console.log(errors);
    console.log("************* LOG ************");
    if (errors.length > 0) {
      return this.setState({
        errors
      });
    }

    let { id, name } = this.state;

    axios
      .put(`${API_HOST}/company/${id}`, { name })
      .then(resp => {
        this.props.history.push("/company");
      })
      .catch(err => alert(err));
  }

  componentDidMount() {
    let { id } = this.props.match.params;

    axios.get(`${API_HOST}/company/${id}`).then(res => {
      const company = res.data;
      this.setState({ ...company });
    });
  }

  render() {
    if (!this.state.id) return "Loading ...";
    return (
      <div className="d-flex justify-content-center">
        <form>
          {this.state.errors.length ? (
            <div className="errors">
              {this.state.errors.map((error, i) => {
                return <p key={"error" + i}>{error}</p>;
              })}
            </div>
          ) : (
            ""
          )}
          <div>
            <label htmlFor="name">Name:</label>
            <div>
              <input
                name="name"
                type="text"
                id="name"
                value={this.state.name}
                onChange={this.handleInputChange}
              />
            </div>
          </div>

          <div>
            <input type="submit" onClick={this.handleSubmit} />
          </div>
        </form>
      </div>
    );
  }
}
