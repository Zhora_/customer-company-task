import React from "react";
import axios from "axios";
import { API_HOST } from "../config";

export default class CreateCompany extends React.Component {
  state = {
    companies: [],
    errors: []
  };

  constructor(props) {
    super(props);

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  validateForm() {
    let errors = [];

    let { name } = this.state;

    if (!name) {
      errors.push("Name is required!");
    }

    return errors;
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleSubmit(event) {
    event.preventDefault();

    let errors = this.validateForm();

    if (errors.length > 0) {
      return this.setState({
        errors
      });
    }

    let { name } = this.state;

    axios
      .post(`${API_HOST}/company`, { name })
      .then(resp => {
        this.props.history.push("/company");
      })
      .catch(err => alert(err));
  }

  render() {
    return (
      <div className="d-flex justify-content-center">
        <form>
          {this.state.errors.length ? (
            <div className="errors">
              {this.state.errors.map((error, i) => {
                return <p key={"error" + i}>{error}</p>;
              })}
            </div>
          ) : (
            ""
          )}
          <div>
            <label htmlFor="name">Name:</label>
            <div>
              <input
                name="name"
                type="text"
                id="name"
                onChange={this.handleInputChange}
              />
            </div>
          </div>

          <div>
            <input type="submit" onClick={this.handleSubmit} />
          </div>
        </form>
      </div>
    );
  }
}
