import React from "react";
import axios from "axios";
import Pagination from "react-js-pagination";
import { API_HOST } from "../config";
import moment from "moment";
import { Link } from "react-router-dom";

export default class Company extends React.Component {
  state = {
    companies: [],
    activePage: 1,
    totalCount: null
  };

  constructor(props) {
    super(props);

    this.handlePageChange = this.handlePageChange.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }

  componentDidMount() {
    this.getCompanies(1);
  }

  getCompanies(page) {
    axios.get(`${API_HOST}/company?page=${page}`).then(res => {
      const { data: companies, totalCount } = res.data;
      this.setState({ companies, activePage: page, totalCount });
    });
  }

  deleteCompany(id) {
    axios.delete(`${API_HOST}/company/${id}`).then(res => {
      this.getCompanies(this.state.activePage);
    });
  }

  handlePageChange(pageNumber) {
    this.getCompanies(pageNumber);
  }

  handleDelete(id) {
    this.deleteCompany(id);
  }

  render() {
    return (
      <div>
        <div className="d-flex justify-content-end mb-20">
          <div>
            <Link to="/company/create">
              <button className="btn-success"> Add Company</button>
            </Link>
          </div>
        </div>
        <section className="grid-container">
          {!this.state.companies.length ? (
            <div>No Companies found! </div>
          ) : (
            this.state.companies.map(company => (
              <article key={company.id}>
                <div className="text">
                  <p className="card-date">
                    Date created:{" "}
                    {moment(company.created_at).format("YYYY-MM-DD")}
                  </p>
                  <h5>Company: {company.name}</h5>
                  <Link to={"/company/update/" + company.id}>
                    <button className="btn-update">Update</button>
                  </Link>
                  <button
                    className="btn-delete"
                    onClick={() => this.handleDelete(company.id)}
                  >
                    Delete
                  </button>{" "}
                </div>
              </article>
            ))
          )}
        </section>

        <Pagination
          hideFirstLastPages={true}
          activePage={this.state.activePage}
          itemsCountPerPage={4}
          totalItemsCount={this.state.totalCount}
          pageRangeDisplayed={5}
          itemClass="page-item"
          linkClass="page-link"
          innerClass="pagination d-flex justify-content-center"
          onChange={this.handlePageChange}
        />
      </div>
    );
  }
}
