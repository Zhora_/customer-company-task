import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

//routes
import Company from "./Company";
import Customer from "./Customer";
import UpdateCustomer from "./Customer/update";
import UpdateCompany from "./Company/update";
import CreateCompany from "./Company/create";
import CreateCustomer from "./Customer/create";

//assets
import "./index.css";

ReactDOM.render(
  <Router>
    <div>
      <nav className="mb-20">
        <ul>
          <li>
            <Link to="/">Customers</Link>
          </li>
          <li>
            <Link to="/company">Companies</Link>
          </li>
        </ul>
      </nav>

      <Switch>
        <Route exact path="/" component={Customer} />
        <Route exact path="/customer/create" component={CreateCustomer} />
        <Route exact path="/customer/update/:id" component={UpdateCustomer} />
        <Route exact path="/company" component={Company} />
        <Route exact path="/company/create" component={CreateCompany} />
        <Route exact path="/company/update/:id" component={UpdateCompany} />
      </Switch>
    </div>
  </Router>,
  document.getElementById("root")
);
