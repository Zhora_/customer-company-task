import React from "react";
import axios from "axios";
import { API_HOST } from "../config";

class CreateCustomer extends React.Component {
  state = {
    companies: [],
    errors: []
  };

  constructor(props) {
    super(props);

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  validateForm() {
    let errors = [];

    let { first_name, last_name, company_id } = this.state;

    if (!first_name) {
      errors.push("First name is required.");
    }

    if (!last_name) {
      errors.push("Last name is required.");
    }

    if (!company_id || !parseInt(company_id)) {
      errors.push("Company is required.");
    }

    return errors;
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleSubmit(event) {
    event.preventDefault();

    let errors = this.validateForm();

    if (errors.length) {
      return this.setState({
        errors
      });
    }

    let { first_name, last_name, company_id } = this.state;

    axios
      .post(`${API_HOST}/customer`, { first_name, last_name, company_id })
      .then(resp => {
        this.props.history.push("/");
      })
      .catch(err => alert(err));
  }

  componentDidMount() {
    this.getAllCompanies();
  }

  getAllCompanies() {
    axios.get(`${API_HOST}/company?show_all=true`).then(res => {
      const { data: companies } = res.data;

      this.setState({ companies });
    });
  }

  render() {
    return (
      <div className="d-flex justify-content-center">
        <form>
          {this.state.errors.length ? (
            <div className="errors">
              {this.state.errors.map((error, i) => {
                return <p key={"error" + i}>{error}</p>;
              })}
            </div>
          ) : (
            ""
          )}
          <div>
            <label htmlFor="firs_name">First Name:</label>
            <div>
              <input
                name="first_name"
                type="text"
                id="firs_name"
                onChange={this.handleInputChange}
              />
            </div>
          </div>
          <div>
            <label htmlFor="last_name">Last Name:</label>
            <div>
              <input
                name="last_name"
                type="text"
                id="last_name"
                onChange={this.handleInputChange}
              />
            </div>
          </div>

          <div>
            <label htmlFor="company_id">Company:</label>
            <div>
              <select
                name="company_id"
                id="company_id"
                onChange={this.handleInputChange}
              >
                <option>Select Company</option>
                {this.state.companies.map(company => {
                  return (
                    <option key={company.id} value={company.id}>
                      {company.name}
                    </option>
                  );
                })}
              </select>
            </div>
          </div>

          <div>
            <input type="submit" onClick={this.handleSubmit} />
          </div>
        </form>
      </div>
    );
  }
}

export default CreateCustomer;
