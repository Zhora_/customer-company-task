import React from "react";
import axios from "axios";
import moment from "moment";
import Pagination from "react-js-pagination";
import { Link } from "react-router-dom";

import { API_HOST } from "../config";

export default class Customer extends React.Component {
  state = {
    customers: [],
    companies: [],
    q: null,
    company_id: null,
    activePage: 1,
    totalCount: null
  };

  constructor(props) {
    super(props);

    this.handlePageChange = this.handlePageChange.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  componentDidMount() {
    this.getCustomers(1);
    this.getAllCompanies();
  }

  getAllCompanies() {
    axios.get(`${API_HOST}/company?show_all=true`).then(res => {
      const { data: companies } = res.data;

      this.setState({ companies });
    });
  }

  getCustomers(page) {
    axios.get(`${API_HOST}/customer?page=${page}`).then(res => {
      const { data: customers, totalCount } = res.data;
      this.setState({ customers, activePage: page, totalCount });
    });
  }

  getCustomersBySearch(page, query, company_id) {
    let params = `?page=${page}`;
    if (query) {
      params += `&q=${query}`;
    }
    if (company_id) {
      params += `&company_id=${company_id}`;
    }

    axios.get(`${API_HOST}/customer/_search${params}`).then(res => {
      const { data: customers, totalCount } = res.data;
      this.setState({ customers, activePage: page, totalCount });
    });
  }

  deleteCustomer(id) {
    axios.delete(`${API_HOST}/customer/${id}`).then(res => {
      this.getCustomers(this.state.activePage);
    });
  }

  handlePageChange(pageNumber) {
    if (this.state.q || this.state.company_id) {
      this.getCustomersBySearch(
        pageNumber,
        this.state.q,
        this.state.company_id
      );
    } else {
      this.getCustomers(pageNumber);
    }
  }

  handleDelete(id) {
    this.deleteCustomer(id);
  }

  handleInputChange(event) {
    const target = event.target;
    const name = target.name;
    const value = target.value;

    this.setState(
      {
        [name]: value
      },
      () => {
        this.getCustomersBySearch(
          this.state.activePage,
          this.state.q,
          this.state.company_id
        );
      }
    );
  }

  render() {
    return (
      <div>
        <div id="filter" className="d-flex justify-content-between mb-20">
          <div>
            <label>
              Search:
              <input type="text" name="q" onChange={this.handleInputChange} />
            </label>

            <label>
              <select name="company_id" onChange={this.handleInputChange}>
                <option>Select company</option>
                {this.state.companies.map(company => {
                  return (
                    <option key={company.id} value={company.id}>
                      {company.name}(#{company.id})
                    </option>
                  );
                })}
              </select>
            </label>
          </div>
          <div>
            <Link to="/customer/create">
              <button className="btn-success"> Add Customer</button>
            </Link>
          </div>
        </div>
        <section className="grid-container">
          {!this.state.customers.length ? (
            <div>No Customers found! </div>
          ) : (
            this.state.customers.map(customer => (
              <article key={customer.id}>
                <div className="text">
                  <p className="card-date">
                    Date created:{" "}
                    {moment(customer.created_at).format("YYYY-MM-DD")}
                  </p>
                  <h5>Company: {customer.company_name}</h5>
                  <h5>
                    Customer: {customer.first_name} {customer.last_name}
                  </h5>

                  <Link to={"/customer/update/" + customer.id}>
                    <button className="btn-update">Update</button>
                  </Link>

                  <button
                    className="btn-delete"
                    onClick={() => this.handleDelete(customer.id)}
                  >
                    Delete
                  </button>
                </div>
              </article>
            ))
          )}
        </section>

        <Pagination
          className="d-flex"
          hideFirstLastPages={true}
          activePage={this.state.activePage}
          itemsCountPerPage={4}
          totalItemsCount={this.state.totalCount}
          pageRangeDisplayed={5}
          itemClass="page-item"
          linkClass="page-link"
          innerClass="pagination d-flex justify-content-center"
          onChange={this.handlePageChange}
        />
      </div>
    );
  }
}
